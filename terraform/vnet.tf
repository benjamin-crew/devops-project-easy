resource "azurerm_virtual_network" "vnet" {
  provider            = azurerm.bctech
  name                = "devops-easy-vnet01"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  address_space       = ["172.18.5.0/25"]

  tags = {
    owner = "Benjamin Crew"
    Usage = "vNet for devops-project-easy VMs"
  }
}