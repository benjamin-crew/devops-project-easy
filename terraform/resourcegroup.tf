resource "azurerm_resource_group" "rg" {
  provider = azurerm.bctech
  name     = "devops-easy-rg01"
  location = "uksouth"

  tags = {
    owner = "Benjamin Crew"
    Usage = "Resource Group to store devops-project-easy resources"
  }
}