resource "azurerm_storage_account" "storage-account" {
  provider                 = azurerm.bctech
  name                     = "devopseasysa01"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    owner = "Benjamin Crew"
    Usage = "Storage account to host blob storage for web server files."
  }
}

# Create the storage container to store tfstate.
resource "azurerm_storage_container" "storage-container" {
  provider              = azurerm.bctech
  name                  = "web-server-files"
  storage_account_name  = azurerm_storage_account.storage-account.name
  container_access_type = "private"
}