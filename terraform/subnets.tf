resource "azurerm_subnet" "config-mgmt" {
  provider             = azurerm.bctech
  name                 = "devops-easy-sb01"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["172.18.5.0/27"]
}

resource "azurerm_subnet" "web-servers" {
  provider             = azurerm.bctech
  name                 = "devops-easy-sb02"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["172.18.5.32/27"]
}