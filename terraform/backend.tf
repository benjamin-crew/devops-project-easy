terraform {
  backend "azurerm" {
    resource_group_name  = "bctech-prod-terraform-rg01"
    storage_account_name = "bctechprodterraformsa01"
    container_name       = "tfstate"
    key                  = "devops-easy-resources.tfstate"
  }
}