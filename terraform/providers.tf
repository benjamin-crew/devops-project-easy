provider "azurerm" {
  features {}

  # Specify subscription
  alias           = "bctech"
  subscription_id = var.AZURE_SUBSCRIPTION_ID
  tenant_id       = var.AZURE_TENANT_ID

  # SPN Details
  client_id     = var.AZURE_CLIENT_ID
  client_secret = var.AZURE_CLIENT_SECRET
}