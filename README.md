# devops-project-easy

An easy devops project.

## Tools

- Azure
- Terraform
- Ansible
- Docker
- GitLab CI/CD

## Step-by-Step

1.) Created a service principal in Azure to run the CI/CD pipelines as. Added the SPN details to the GitLab project as variables.
2.) Created the pipeline and deployed the initial terraform environment.
3.) Set the apply stage of the pipeline to only run on main. Protected the main branch. Updates must be merged from a new branch.